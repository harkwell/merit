Amazon Web Services
=================
Overview
---------------
AWS hosts infrastructure to serve software applications.  Those utilizing
the Free Tier service can do so for up to a year.


Deploying Merit to the AWS Cloud
---------------

```shell
aws configure

# create an EC2 keypair (if not already) and note the name
EC2KEYPAIR=merit
cat AWS-cloudformation.json \
   |sed -e 's#MYKEYPAIR#'$EC2KEYPAIR'#g'  \
   |aws cloudformation create-stack --stack-name merit --capabilities CAPABILITY_NAMED_IAM --template-body file:///dev/stdin
```


Delete Merit From the AWS Cloud
---------------

```shell
aws cloudformation delete-stack --stack-name merit
```
