// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.ctrl.Rewards;
import com.khallware.merit.ctrl.Purchases;
import com.khallware.merit.ctrl.Activities;
import com.khallware.merit.ctrl.Participants;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import org.glassfish.grizzly.http.server.HttpServer;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.HashSet;
import java.util.Set;

public class Main
{
	public static final String DEF_URL = "http://localhost:8080";
	public static final String[] PACKAGES = { "com.khallware.merit.ctrl" };
	private static final Logger logger = LoggerFactory.getLogger(
		Main.class);

	public static void main(String... args) throws Exception
	{
		String url = (args.length > 0) ? args[0] : DEF_URL;
		ResourceConfig cfg = new PackagesResourceConfig(PACKAGES) {
			public Set<Class<?>> getClasses()
			{
				Set<Class<?>> retval = new HashSet<>();
				retval.add(Participants.class);
				retval.add(Activities.class);
				retval.add(Purchases.class);
				retval.add(Rewards.class);
				return(retval);
			}
		};
		HttpServer svr = GrizzlyServerFactory.createHttpServer(url,cfg);
		logger.debug("starting grizzly webserver...");
		svr.start();
		System.out.println("press enter to stop...");
		System.in.read();
	}
}
