// Copyright Kevin D.Hall 2017

package com.khallware.merit.ctrl;

import com.khallware.merit.domain.Participant;
import com.khallware.merit.domain.Purchase;
import com.khallware.merit.Datastore;
import com.jayway.jsonpath.JsonPath;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import javax.annotation.Resource;
import javax.ws.rs.core.Response;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;

@Path("/v0.1.0/merit/purchases")
public class Purchases
{
	private static final Logger logger = LoggerFactory.getLogger(
		Purchases.class);

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response handlePost(String json)
	{
		Response retval = null;
		int id = JsonPath.<Integer>read(json, "$.participant");
		Purchase.Builder purchase = new Purchase.Builder();
		Participant.Builder participant = new Participant.Builder();
		participant.id(JsonPath.<Integer>read(json, "$.participant"));
		purchase.reward(JsonPath.read(json, "$.reward"));
		purchase.value(JsonPath.<Integer>read(json, "$.value"));
		logger.trace(json);
		try {
			Datastore.instance().create(purchase.build(),
				participant.build());
			retval = Response.status(200)
				.entity(""+purchase.build()).build();
		}
		catch (Exception e) {
			logger.error(""+e, e);
			retval = Response.status(400).entity(""+e).build();
		}
		return(retval);
	}

	@GET
	@Produces("application/json")
	public Response handleGet()
	{
		Response retval = null;
		StringBuilder sb = new StringBuilder();
		boolean flag = false;
		sb.append("{ \"purchases\" : [ ");
		try {
			for (Purchase purchase :
					Datastore.instance().readPurchases()) {
				sb.append((flag) ? ", " : "");
				sb.append(""+purchase);
				flag = true;
			}
			sb.append(" ] }");
			retval = Response.status(200).entity(""+sb).build();
		}
		catch (Exception e) {
			logger.error(""+e, e);
			retval = Response.status(400).entity(""+e).build();
		}
		return(retval);
	}

	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	public Response handlePut(String json)
	{
		Response retval = null;
		logger.trace(json);
		try {
			String date = JsonPath.read(json, "$.date");
			Datastore.instance().deletePurchase(date);
			retval = handlePost(json);
		}
		catch (Exception e) {
			logger.error(""+e, e);
			retval = Response.status(400).entity(""+e).build();
		}
		return(retval);
	}

	@DELETE
	@Consumes("application/json")
	@Produces("application/json")
	public Response handleDelete(String json)
	{
		Response retval = null;
		Purchase purchase = null;
		logger.trace(json);
		try {
			String date = JsonPath.read(json, "$.date");
			purchase = Datastore.instance().readPurchase(date);
			retval = Response.status(200)
				.entity(""+purchase).build();
			Datastore.instance().deletePurchase(date);
		}
		catch (Exception e) {
			logger.error(""+e, e);
			retval = Response.status(400).entity(""+e).build();
		}
		return(retval);
	}
}
