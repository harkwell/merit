// Copyright Kevin D.Hall 2017

package com.khallware.merit.ctrl;

import com.khallware.merit.domain.Activity;
import com.khallware.merit.Datastore;
import com.jayway.jsonpath.JsonPath;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import javax.annotation.Resource;
import javax.ws.rs.core.Response;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;

@Path("/v0.1.0/merit/activities")
public class Activities
{
	private static final Logger logger = LoggerFactory.getLogger(
		Activities.class);

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response handlePost(String json)
	{
		Response retval = null;
		Activity.Builder activity = new Activity.Builder();
		activity.name(JsonPath.read(json, "$.name"));
		activity.description(JsonPath.read(json, "$.description"));
		activity.value(JsonPath.<Integer>read(json, "$.value"));
		logger.trace(json);
		try {
			Datastore.instance().create(activity.build());
			retval = Response.status(200)
				.entity(""+activity.build()).build();
		}
		catch (Exception e) {
			logger.error(""+e, e);
			retval = Response.status(400).entity(""+e).build();
		}
		return(retval);
	}

	@GET
	@Produces("application/json")
	public Response handleGet()
	{
		Response retval = null;
		StringBuilder sb = new StringBuilder();
		boolean flag = false;
		sb.append("{ \"activities\" : [ ");
		try {
			for (Activity activity :
					Datastore.instance().readActivities()) {
				sb.append((flag) ? ", " : "");
				sb.append(""+activity);
				flag = true;
			}
			sb.append(" ] }");
			retval = Response.status(200).entity(""+sb).build();
		}
		catch (Exception e) {
			logger.error(""+e, e);
			retval = Response.status(400).entity(""+e).build();
		}
		return(retval);
	}

	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	public Response handlePut(String json)
	{
		Response retval = null;
		logger.trace(json);
		try {
			String name = JsonPath.read(json, "$.name");
			Datastore.instance().deleteActivity(name);
			retval = handlePost(json);
		}
		catch (Exception e) {
			logger.error(""+e, e);
			retval = Response.status(400).entity(""+e).build();
		}
		return(retval);
	}

	@DELETE
	@Consumes("application/json")
	@Produces("application/json")
	public Response handleDelete(String json)
	{
		Response retval = null;
		Activity activity = null;
		logger.trace(json);
		try {
			String name = JsonPath.read(json, "$.name");
			activity = Datastore.instance().readActivity(name);
			retval = Response.status(200)
				.entity(""+activity).build();
			Datastore.instance().deleteActivity(name);
		}
		catch (Exception e) {
			logger.error(""+e, e);
			retval = Response.status(400).entity(""+e).build();
		}
		return(retval);
	}
}
