// Copyright Kevin D.Hall 2017

package com.khallware.merit.ctrl;

import com.khallware.merit.domain.Participant;
import com.khallware.merit.Datastore;
import com.jayway.jsonpath.JsonPath;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import javax.annotation.Resource;
import javax.ws.rs.core.Response;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;

@Path("/v0.1.0/merit/participants")
public class Participants
{
	private static final Logger logger = LoggerFactory.getLogger(
		Participants.class);

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response handlePost(String json)
	{
		Response retval = null;
		Participant.Builder participant = new Participant.Builder();
		participant.name(JsonPath.read(json, "$.name"));
		logger.trace(json);
		try {
			Datastore.instance().create(participant.build());
			retval = Response.status(200)
				.entity(""+participant.build()).build();
		}
		catch (Exception e) {
			logger.error(""+e, e);
			retval = Response.status(400).entity(""+e).build();
		}
		return(retval);
	}

	@GET
	@Produces("application/json")
	public Response handleGet()
	{
		Response retval = null;
		StringBuilder sb = new StringBuilder();
		boolean flag = false;
		sb.append("{ \"participants\" : [ ");
		try {
			for (Participant participant : Datastore.instance()
					.readParticipants()) {
				sb.append((flag) ? ", " : "");
				sb.append(""+participant);
				flag = true;
			}
			sb.append(" ] }");
			retval = Response.status(200).entity(""+sb).build();
		}
		catch (Exception e) {
			logger.error(""+e, e);
			retval = Response.status(400).entity(""+e).build();
		}
		return(retval);
	}

	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	public Response handlePut(String json)
	{
		String msg = "Once added, a participant may not be modified.";
		logger.trace(json);
		return(Response.status(400).entity(
			""+(new IllegalStateException(msg))).build());
	}

	@DELETE
	@Consumes("application/json")
	@Produces("application/json")
	public Response handleDelete(String json)
	{
		Response retval = null;
		Participant participant = null;
		logger.trace(json);
		try {
			String name = JsonPath.read(json, "$.name");
			participant =Datastore.instance().readParticipant(name);
			retval = Response.status(200)
				.entity(""+participant).build();
			Datastore.instance().deleteParticipant(name);
		}
		catch (Exception e) {
			logger.error(""+e, e);
			retval = Response.status(400).entity(""+e).build();
		}
		return(retval);
	}
}
