Khallware Merit Service
=================
Overview
---------------
This code implements a classic Merit/Demerit system.  It consists of a java
RESTful webservice (stand-alone) and an android-tv app front-end.  It basically
empowers parents/leaders to define activities/rewards and then reports history
and sums for children/participants.


Building the Merit Core Library
---------------

```shell
# choose a location to keep application library build/run dependencies
export MERIT_MAVEN_REPO=/tmp/foo
rm -rf $MERIT_MAVEN_REPO

# select and enable "java 8", checkout and build
update-alternatives --config java
git clone https://gitlab.com/harkwell/merit.git && cd merit/merit-lib
mvn -Dmaven.repo.local=$MERIT_MAVEN_REPO package
ls -ld target/Merit.jar

# install the merit library artifact to $MERIT_MAVEN_REPO
mvn install:install-file -Dmaven.repo.local=$MERIT_MAVEN_REPO -Dfile=target/Merit.jar -DgroupId=com.khallware.merit -DartifactId=khallware-merit -Dversion=0.1 -Dpackaging=jar
```


Building the Android App
---------------

```shell
# choose the ip address of the service host (eg this build host)
export IP_ADDR=$(ip addr show eth0 |grep 'inet ' |awk '{ print $2 }' |cut -d/ -f1)
export ANDROID_HOME=$HOME/Android/Sdk/

# build and copy to a temporary location
cd ../android
sed -in -e 's#^sdk.dir.*$#sdk.dir='$ANDROID_HOME'#' local.properties
sed -in -e 's#192.168.23.103#'$IP_ADDR'#g' src/*/java/*/*/*/MeritFetcher.java
mvn -Dmaven.repo.local=$MERIT_MAVEN_REPO package
cp target/Merit.apk /tmp/$(date +%Y%m%d)-Merit.apk
```


Building the WebService
---------------

```shell
cd ../merit-ws
mvn -Dmaven.repo.local=$MERIT_MAVEN_REPO \
    org.apache.maven.plugins:maven-dependency-plugin:2.1:get \
    -DrepoUrl=https://mvnrepository.com/ \
    -Dartifact=org.apache.derby:derby:10.8.3.0
mvn -Dmaven.repo.local=$MERIT_MAVEN_REPO clean package
ls -ld target/merit.jar
export CLASSPATH=$(find $MERIT_MAVEN_REPO/ -name \*jar |xargs |tr ' ' :):$PWD/target/merit.jar
java -Dlog4j.configuration=file://$PWD/log4j.properties com.khallware.merit.Main http://$IP_ADDR:8080

# tests...
curl -i -X POST -H "Accept:application/json" -H "Content-Type:application/json" http://$IP_ADDR:8080/v0.1.0/merit/participants -d '{"name":"child1"}'
curl -i -X POST -H "Accept:application/json" -H "Content-Type:application/json" http://$IP_ADDR:8080/v0.1.0/merit/activities -d '{"name":"sweep","description":"sweep under the kitchen table","value":5}'
curl -i -X POST -H "Accept:application/json" -H "Content-Type:application/json" http://$IP_ADDR:8080/v0.1.0/merit/rewards -d '{"name":"video game time","description":"an extra half our of video game time","value":25}'
curl -i -X POST -H "Accept:application/json" -H "Content-Type:application/json" http://$IP_ADDR:8080/v0.1.0/merit/purchases -d '{"participant":1,"reward":"an extra half our of video game time","value":25}'

for x in activities participants rewards purchases; do
   curl -s -H "Accept:application/json" http://$IP_ADDR:8080/v0.1.0/merit/$x |jq '.'
done
curl -i -X PUT -H "Accept:application/json" -H "Content-Type:application/json" http://$IP_ADDR:8080/v0.1.0/merit/activities -d '{"name":"sweep","description":"sweep under the dining table","value":3}'
curl -i -X PUT -H "Accept:application/json" -H "Content-Type:application/json" http://$IP_ADDR:8080/v0.1.0/merit/rewards -d '{"name":"video game time","description":"an extra half-hour of video game time","value":30}'
DATE=$(curl -s -H Accept:application/json    http://$IP_ADDR:8080/v0.1.0/merit/purchases |jq '.purchases[0].date' |sed 's#"##g')
echo curl -i -X PUT -H "Accept:application/json" -H "Content-Type:application/json" http://$IP_ADDR:8080/v0.1.0/merit/purchases -d \''{"participant":1,"reward":"an extra half-hour of video game time","value":30,"date":"'$DATE'"}'\' |bash
```

Running the Android App
---------------

```shell
export ANDROID_HOME=$HOME/Android/Sdk/
export PATH=$PATH:$ANDROID_HOME/emulator:$ANDROID_HOME/tools
android list target
emulator -list-avds
emulator -avd Android_TV_1080p_API_25
adb devices
adb uninstall com.khallware.merit
adb install /tmp/*Merit.apk
adb shell am start -n "com.khallware.merit/.Merit"
adb logcat
```


Database Maintenance
---------------

```shell
### dump (backups)
chromium-browser https://db.apache.org/derby/docs/10.8/adminguide/tadminexporting.html
cd . # location of merit.derby (merit/merit-ws/ from git clone)
echo "CONNECT 'jdbc:derby:merit.derby';" >merit-dump.sql

for t in participants activities rewards purchases; do
   echo "CALL SYSCS_UTIL.SYSCS_EXPORT_TABLE(null,'"$(echo $t |tr a-z A-Z)"','merit-"$t"-dump.csv',null,null,null);" >>merit-dump.sql
done
ij merit-dump.sql && rm merit-dump.sql
tar zcvf $(date +%Y%m%d)-merit-dump.tgz merit*dump.csv && rm merit*dump.csv


### load (prepare for full restore)
# drop data from all tables (or create schema by hand)
cd . # location of merit.derby (merit/merit-ws/ from git clone)
echo "CONNECT 'jdbc:derby:merit.derby';" >merit-trunc-tables.sql

for t in activities rewards purchases participants; do
   echo "DELETE FROM $t;" >>merit-trunc-tables.sql
done
ij merit-trunc-tables.sql && rm merit-trunc-tables.sql


### load (restore)
tar zxvf *-merit-dump.tgz
echo "CONNECT 'jdbc:derby:merit.derby;';" >merit-load.sql

for t in participants activities rewards purchases; do
   while IFS='' read -r line || [[ -n "$line" ]]; do
      echo 'INSERT INTO '$t' VALUES ('$line');' >>merit-load.sql
   done <"merit-$t-dump.csv"
done
sed -in -e 's#"#\x27#g' merit-load.sql
ij merit-load.sql && rm merit-load.sql merit-*-dump.csv
```


One-Time-Only (Android SDK)
---------------

```shell
chromium-browser https://developer.android.com/studio/index.html
# or follow similar below...
mkdir -p $HOME/Android && cd $HOME/Android && unzip ~/Downloads/android-studio-ide*zip && rm ~/Downloads/android-studio-ide*zip
./android-studio/bin/studio.sh
# "I do not have a previos version..."
# Next -> Standard -> Next -> Next -> Finish
```


One-Time-Only (Derby Utilities)
---------------

```shell
yum install -y wget unzip
wget http://tux.rainside.sk/apache//db/derby/db-derby-10.13.1.1/db-derby-10.13.1.1-bin.zip -O /tmp/derby.zip
mkdir -p /usr/local && unzip -d /usr/local /tmp/derby.zip
mv /usr/local/db-derby* /usr/local/derby
export PATH=$PATH:/usr/local/derby/bin
mkdir -p /derby && cd /derby
```


One-Time-Only (Amazon EC2/Centos7)
---------------

```shell
yum install -y git java-1.8.0-openjdk-devel jq
wget -c http://mirror.stjschools.org/public/apache/maven/maven-3/3.5.0/binaries/apache-maven-3.5.0-bin.tar.gz -O /tmp/maven.tgz
tar zxvf /tmp/maven.tgz -C /usr/local
export PATH=$PATH:/usr/local/apache-maven-3.5.0/bin
```
