// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertTrue;

public class UtilTest
{
	private static final Logger logger = LoggerFactory.getLogger(
		UtilTest.class);

	@Test
	public void flatJsonArrayParserHappyPath() throws Exception
	{
		String json = "[ { 'name' : 'foo', 'junk' : -1 }, "
			+"{ 'name' : 'bar', 'junk' : 'twelve' } ]";
		enforce(json, 2);

		json = " [ { 'key1' : 'val1', 'key2' : 1 }, "
			+"{ 'key3' : { 'val3', 'key4' : 'val4' }, "
			+"{ some weird, non-json string... }, "
			+"{ 'key7' : 'val7', 'key8' : 'val8' } ]   ";
		enforce(json, 4);

		json = "[]";
		enforce(json, 0);
	}

	@Test
	public void flatJsonArrayParserSadPath() throws Exception
	{
		String json = "[ { 'name' : 'foo', 'junk' : -1 }, "
			+"{ 'name' : 'bar', 'junk' : 'twelve' ]";
		boolean flag = true;
		try {
			enforce(json, 2);
			flag = false;
		}
		catch (MeritException e) {
			System.out.printf("EXPECTED EXCEPTION:\n%s\n", ""+e);
		}
		try {
			json = "[";
			enforce(json, 0);
			flag = false;
		}
		catch (MeritException e) {
			System.out.printf("EXPECTED EXCEPTION:\n%s\n", ""+e);
		}
		assertTrue(flag);
	}

	private static void enforce(String json, int answer) throws Exception
	{
		int count = 0;

		for (String token : Util.flatJsonArrayParser(json)) {
			count++;
			logger.info("token = \"{}\"", token);
		}
		assertTrue((count == answer));
	}
}
