// Copyright Kevin D.Hall 2017

package com.khallware.merit.domain;

public class Activity
{
	public static final class Builder
	{
		Activity activity = null;

		public Builder()
		{
			activity = new Activity();
		}

		public Builder name(String name)
		{
			activity.name = name;
			return(this);
		}

		public Builder description(String description)
		{
			activity.description = description;
			return(this);
		}

		public Builder value(int value)
		{
			activity.value = value;
			return(this);
		}

		public Activity build()
		{
			return(activity);
		}
	}

	protected String name;
	protected String description;
	protected int value;

	public Activity()
	{
	}

	public String getName()
	{
		return(name);
	}

	public String getDescription()
	{
		return(description);
	}

	public int getValue()
	{
		return(value);
	}

	@Override
	public String toString()
	{
		String format = "{ \"name\" : \"%s\", "
			+"\"description\" : \"%s\", \"value\" : %d }";
		return(String.format(format, name, description, value));
	}
}
