// Copyright Kevin D.Hall 2017

package com.khallware.merit.domain;

public class Reward
{
	public static final class Builder
	{
		Reward reward = null;

		public Builder()
		{
			reward = new Reward();
		}

		public Builder name(String name)
		{
			reward.name = name;
			return(this);
		}

		public Builder description(String description)
		{
			reward.description = description;
			return(this);
		}

		public Builder value(int value)
		{
			reward.value = value;
			return(this);
		}

		public Reward build()
		{
			return(reward);
		}
	}

	protected String name;
	protected String description;
	protected int value;

	public Reward()
	{
	}

	public String getName()
	{
		return(name);
	}

	public String getDescription()
	{
		return(description);
	}

	public int getValue()
	{
		return(value);
	}

	@Override
	public String toString()
	{
		String format = "{ \"name\" : \"%s\", "
			+"\"description\" : \"%s\", \"value\" : %d }";
		return(String.format(format, name, description, value));
	}
}
