// Copyright Kevin D.Hall 2017

package com.khallware.merit.domain;

import com.khallware.merit.MeritException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;

public class Purchase
{
	public static final class Builder
	{
		Purchase purchase = null;

		public Builder()
		{
			purchase = new Purchase();
		}

		public Builder participant(String participant)
		{
			purchase.participant = participant;
			return(this);
		}

		public Builder date(Date date)
		{
			purchase.date = date;
			return(this);
		}

		public Builder date(String date) throws MeritException
		{
			try {
				purchase.date = DateFormat.getInstance().parse(
					date);
			}
			catch (Exception e) {
				throw new MeritException(e);
			}
			return(this);
		}

		public Builder reward(String reward)
		{
			purchase.reward = reward;
			return(this);
		}

		public Builder value(int value)
		{
			purchase.value = value;
			return(this);
		}

		public Purchase build()
		{
			return(purchase);
		}
	}

	protected String participant;
	protected String reward;
	protected Date date;
	protected int value;

	public Purchase()
	{
	}

	public String getParticipant()
	{
		return(participant);
	}

	public String getReward()
	{
		return(reward);
	}

	public int getValue()
	{
		return(value);
	}

	@Override
	public String toString()
	{
		String FMT = "yyyy-MM-dd HH:mm:ss.SS";
		String format = "{ \"participant\" : \"%s\", "
			+"\"reward\" : \"%s\", \"value\" : %d, "
			+"\"date\" : \"%s\" }";
		return(String.format(format, participant, reward, value,
			(date != null)
				? new SimpleDateFormat(FMT).format(date)
				: ""));
	}
}
