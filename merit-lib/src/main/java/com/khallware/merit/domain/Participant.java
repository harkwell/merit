// Copyright Kevin D.Hall 2017

package com.khallware.merit.domain;

public class Participant
{
	public static final class Builder
	{
		Participant participant = null;

		public Builder()
		{
			participant = new Participant();
		}

		public Builder id(int id)
		{
			participant.id = id;
			return(this);
		}

		public Builder balance(int balance)
		{
			participant.balance = balance;
			return(this);
		}

		public Builder name(String name)
		{
			participant.name = name;
			return(this);
		}

		public Participant build()
		{
			return(participant);
		}
	}

	protected int id;
	protected int balance;
	protected String name;

	public Participant()
	{
	}

	public int getId()
	{
		return(id);
	}

	public int getBalance()
	{
		return(balance);
	}

	public String getName()
	{
		return(name);
	}

	@Override
	public String toString()
	{
		String format = "{ \"id\" : %d, \"name\" : \"%s\", "
			+" \"balance\" : %d }";
		return(String.format(format, id, name, balance));
	}
}
