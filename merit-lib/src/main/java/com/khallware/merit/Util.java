// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import java.util.ArrayList;
import java.util.List;

public class Util
{
	public static List<String> flatJsonArrayParser(String json)
			throws MeritException
	{
		List<String> retval = new ArrayList<>();
		int len = json.length();
		int ptr1 = json.indexOf("[");
		int ptr2 = json.lastIndexOf("]");

		if (ptr2 < 0 || ptr1 < 0) {
			throw new MeritException("invalid json array");
		}
		ptr2 = ptr1;

		while (true) {
			if ((ptr1 = json.indexOf("{", ptr2)) < 0) {
				break;
			}
			if ((ptr2 = json.indexOf("}", ptr1)) < 0) {
				throw new MeritException("invalid json");
			}
			retval.add(json.substring(ptr1, Math.min(++ptr2, len)));
		}
		return(retval);
	}

	public static String sanitize(String input) throws MeritException
	{
		String retval = input.replaceAll("\\{","\\\\x7b")
			.replaceAll("\\}","\\\\x7d")
			.replaceAll("\\[","\\\\x5b")
			.replaceAll("\\]","\\\\x5d");
		return(retval);
	}
}
