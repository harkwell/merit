// Copyright Kevin D.Hall 2017

package com.khallware.merit;

public class MeritException extends Exception
{
	private static final long serialVersionUID = 0x0001L;

	public MeritException() {}

	public MeritException(String message)
	{
		super(message);
	}

	public MeritException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public MeritException(Throwable cause)
	{
		super(cause);
	}
}
