BUILD
=================

```shell
export ANDROID_HOME=/usr/local/adt-bundle-linux-x86_64-20140702/sdk/
git clone https://gitlab.com/harkwell/merit.git && cd merit/android
mvn -Dmaven.repo.local=/tmp/foo package && ls -ld target/Merit.apk
rm -rf /tmp/foo
```

USING IDE
=================

```shell
rm -rf ~/StudioProjects/ ~/.AndroidStudio*
mkdir -p src/main/libs/ && cp `find /tmp/foo -name \*jar` src/main/libs/
cp /tmp/foo/org/slf4j/slf4j-android/1.6.1-RC1/slf4j-android-1.6.1-RC1.jar src/main/libs/
cp /tmp/foo/com/khallware/merit/khallware-merit/0.1/khallware-merit-0.1.jar src/main/libs/
$HOME/Android/android-studio/bin/studio.sh

Import the project:
* "I do not have a previous version...do not want to import..."
* next -> standard -> next -> finish -> finish
* Import project
* Select pom.xml -> next -> finish
* Build -> Build APK
```
