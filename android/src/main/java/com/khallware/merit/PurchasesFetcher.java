// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.domain.Purchase;
import com.jayway.jsonpath.JsonPath;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class PurchasesFetcher extends MeritFetcher<Purchase>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		PurchasesFetcher.class);

	public PurchasesFetcher(FetcherUiUpdater<Purchase> updater)
	{
		super(updater);
	}

	@Override
	public String getURI()
	{
		return("/purchases");
	}

	@Override
	public Purchase parse(String json) throws MeritException
	{
                Purchase.Builder retval = new Purchase.Builder();
		try {
			retval.reward(""+JsonPath.read(json, "$.reward"));
			retval.participant(
				""+JsonPath.read(json, "$.participant"));
			retval.value(
				JsonPath.<Integer>read(json, "$.value"));
		}
		catch (Exception e) {
			throw new MeritException("bad json \""+json+"\"",e);
		}
		return(retval.build());
	}
}
