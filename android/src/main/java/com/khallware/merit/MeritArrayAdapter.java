// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import android.widget.ArrayAdapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.content.Context;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.List;

public abstract class MeritArrayAdapter<T> extends ArrayAdapter<T>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		MeritArrayAdapter.class);
	private Context ctxt = null;
	private int layout_id = -1;
	private List<T> list = null;

	public MeritArrayAdapter(Context ctxt, int layout_id, List<T> list)
	{
		super(ctxt, layout_id, list);
		this.layout_id = layout_id;
		this.list = list;
		this.ctxt = ctxt;
	}

	public abstract void fillView(View view, T item);

	public int getCount()
	{
		return(list.size());
	}

	public T getItem(int idx)
	{
		return(list.get(idx));
	}

	public long getItemId(int idx)
	{
		return(list.get(idx).hashCode());
	}

	@Override
	public View getView(int idx, View view, ViewGroup parent)
	{
		View retval = view;

		if (retval == null) {
			LayoutInflater inflater = (LayoutInflater)
				ctxt.getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			retval = inflater.inflate(layout_id, null);
		}
		fillView(retval, getItem(idx));
		return(retval);
	}
}
