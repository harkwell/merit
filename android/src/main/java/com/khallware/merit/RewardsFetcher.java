// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.domain.Reward;
import com.jayway.jsonpath.JsonPath;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class RewardsFetcher extends MeritFetcher<Reward>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		RewardsFetcher.class);

	public RewardsFetcher(FetcherUiUpdater<Reward> updater)
	{
		super(updater);
	}

	@Override
	public String getURI()
	{
		return("/rewards");
	}

	@Override
	public Reward parse(String json) throws MeritException
	{
                Reward.Builder retval = new Reward.Builder();
		try {
			logger.debug("sanitized: {}", json);
			retval.name(""+JsonPath.read(json, "$.name"));
			retval.description(
				""+JsonPath.read(json, "$.description"));
			retval.value(JsonPath.<Integer>read(json, "$.value"));
		}
		catch (Exception e) {
			throw new MeritException("bad json \""+json+"\"",e);
		}
		return(retval.build());
	}
}
