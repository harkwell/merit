// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.domain.Participant;
import com.jayway.jsonpath.JsonPath;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class ParticipantsFetcher extends MeritFetcher<Participant>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		ParticipantsFetcher.class);

	public ParticipantsFetcher(FetcherUiUpdater<Participant> updater)
	{
		super(updater);
	}

	@Override
	public String getURI()
	{
		return("/participants");
	}

	@Override
	public Participant parse(String json) throws MeritException
	{
                Participant.Builder retval = new Participant.Builder();
		try {
			retval.name(""+JsonPath.read(json, "$.name"));
			retval.balance(
				JsonPath.<Integer>read(json, "$.balance"));
		}
		catch (Exception e) {
			throw new MeritException("bad json \""+json+"\"",e);
		}
		return(retval.build());
	}
}
