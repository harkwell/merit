// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.domain.Purchase;
import android.app.ProgressDialog;
import android.app.ListActivity;
import android.os.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PurchasesActivity extends ListActivity
		implements FetcherUiUpdater<Purchase>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		PurchasesActivity.class);
	private PurchasesArrayAdapter adapter = null;
	private ProgressDialog dialog = null;

	@Override
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		adapter = new PurchasesArrayAdapter(this);
		final FetcherUiUpdater updater = this;
		runOnUiThread(new Runnable() {
			public void run()
			{
				new PurchasesFetcher(updater).execute();
			}
		});
	}

	@Override
	public void beforeFetch()
	{
		dialog = new ProgressDialog(this);
		dialog.setTitle("Purchases");
		dialog.setMessage("Loading...");
		dialog.setIndeterminate(false);
		dialog.show();
	}

	@Override
	public void onItemFetch(Purchase purchase)
	{
		adapter.add(purchase);
	}

	@Override
	public void afterFetch()
	{
		dialog.dismiss();
		setListAdapter(adapter);
	}
}
