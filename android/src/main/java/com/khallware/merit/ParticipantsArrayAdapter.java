// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.domain.Participant;
import android.view.View;
import android.widget.TextView;
import android.content.Context;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.List;
import java.util.ArrayList;

public class ParticipantsArrayAdapter extends MeritArrayAdapter<Participant>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		ParticipantsArrayAdapter.class);

	public ParticipantsArrayAdapter(Context ctxt)
	{
		super(ctxt, R.layout.participants_row,
			new ArrayList<Participant>());
	}

	@Override
	public void fillView(View view, Participant participant)
	{
		// name
		TextView textView = (TextView)view.findViewById(
			R.id.participant_list_row_name);
		textView.setText(participant.getName());

		// balance
		textView = (TextView)view.findViewById(
			R.id.participant_list_row_balance);
		textView.setText(""+participant.getBalance());
	}
}
