// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import android.widget.ImageButton;
import android.widget.Toast;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.KeyEvent;
import android.content.Intent;
import android.graphics.Color;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.Map;
import java.util.HashMap;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;

public class Merit extends Activity
{
	private static final Logger logger = LoggerFactory.getLogger(	
		Merit.class);
	private int x_pos = 0;
	private int y_pos = 0;

	@Override
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.main);
	}

	public void launchRewardsActivity(View view)
	{
		logger.trace("launchRewardsActivity()...");
		launchIntent(RewardsActivity.class);
	}

	public void launchActivitiesActivity(View view)
	{
		logger.trace("launchActivitiesActivity()...");
		launchIntent(ActivitiesActivity.class);
	}

	public void launchParticipantsActivity(View view)
	{
		logger.trace("launchParticipantsActivity()...");
		launchIntent(ParticipantsActivity.class);
	}

	public void launchPurchasesActivity(View view)
	{
		logger.trace("launchPurchasesActivity()...");
		launchIntent(PurchasesActivity.class);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		ImageButton button = (ImageButton)findViewById(currentButton());
		boolean pressed = false;
		boolean handled = true;
		button.clearColorFilter();

		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_LEFT:
			x_pos = (--x_pos % 2);
			break;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			x_pos = (++x_pos % 2);
			break;
		case KeyEvent.KEYCODE_DPAD_UP:
			y_pos = (--y_pos % 2);
			break;
		case KeyEvent.KEYCODE_DPAD_DOWN:
			y_pos = (++y_pos % 2);
			break;
		case KeyEvent.KEYCODE_DPAD_CENTER:
			pressed = true;
			break;
		default:
			handled = false;
			break;
		}
		button = (ImageButton)findViewById(currentButton());
		button.requestFocus();
		// http://hex2rgba.devoth.com/ #51748e = rgb(81,116,142)
		// alpha(0...255) 0=transparent 255=opaque
		button.setColorFilter(Color.argb(64, 255, 255, 255));

		if (pressed) {
			button.performClick();
		}
		return(handled || super.onKeyDown(keyCode, event));
	}

	private int currentButton()
	{
		int retval = R.id.btn_rewards;

		switch (y_pos) {
		case 0:
			switch (x_pos) {
			case 0:
				retval = R.id.btn_rewards;
				break;
			case 1:
				retval = R.id.btn_activities;
				break;
			}
			break;
		case 1:
			switch (x_pos) {
			case 0:
				retval = R.id.btn_participants;
				break;
			case 1:
				retval = R.id.btn_purchases;
				break;
			}
			break;
		}
		return(retval);
	}

	protected void launchIntent(Class clazz)
	{
		launchIntent(clazz, new HashMap<String, String>());
	}

	protected void launchIntent(Class clazz, Map<String, String> map)
	{
		try {
			Intent intent = new Intent(this, clazz);

			for (String key : map.keySet()) {
				intent.putExtra(key, map.get(key));
			}
			startActivity(intent);
		}
		catch (Exception e) {
			int duration = Toast.LENGTH_SHORT;
			String msg = toStringWithStacktrace(e);
			Toast toast = Toast.makeText(this, msg, duration);
			logger.error(""+e, e);
			toast.show();
		}
	}

	protected static String toStringWithStacktrace(Exception exception)
	{
		StringBuilder retval = new StringBuilder();
		ByteArrayOutputStream bos = null;
		retval.append(exception.getMessage());
		bos = new ByteArrayOutputStream();
		exception.printStackTrace(new PrintStream(bos));
		retval.append("\n");
		retval.append(""+bos);
		return(""+retval);
	}
}
