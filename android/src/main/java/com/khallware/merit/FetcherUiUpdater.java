// Copyright Kevin D.Hall 2017

package com.khallware.merit;

public interface FetcherUiUpdater<T>
{
	public void beforeFetch();
	public void onItemFetch(T item);
	public void afterFetch();
}
