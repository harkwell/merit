// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.domain.Reward;
import android.view.View;
import android.widget.TextView;
import android.content.Context;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.List;
import java.util.ArrayList;

public class RewardsArrayAdapter extends MeritArrayAdapter<Reward>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		RewardsArrayAdapter.class);

	public RewardsArrayAdapter(Context ctxt)
	{
		super(ctxt, R.layout.rewards_row, new ArrayList<Reward>());
	}

	@Override
	public void fillView(View view, Reward reward)
	{
		// value
		TextView textView = (TextView)view.findViewById(
			R.id.reward_list_row_value);
		textView.setText(""+reward.getValue());

		// name
		textView = (TextView)view.findViewById(
			R.id.reward_list_row_name);
		textView.setText(reward.getName());

		// description
		textView = (TextView)view.findViewById(
			R.id.reward_list_row_desc);
		textView.setText(reward.getDescription());
	}
}
