// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.domain.Purchase;
import android.view.View;
import android.widget.TextView;
import android.content.Context;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.List;
import java.util.ArrayList;

public class PurchasesArrayAdapter extends MeritArrayAdapter<Purchase>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		PurchasesArrayAdapter.class);

	public PurchasesArrayAdapter(Context ctxt)
	{
		super(ctxt, R.layout.purchases_row, new ArrayList<Purchase>());
	}

	@Override
	public void fillView(View view, Purchase purchase)
	{
		TextView textView = (TextView)view.findViewById(
			R.id.purchase_list_row_reward);
		textView.setText(""+purchase.getReward());

		textView = (TextView)view.findViewById(
			R.id.purchase_list_row_participant);
		textView.setText(""+purchase.getParticipant());

		textView = (TextView)view.findViewById(
			R.id.purchase_list_row_value);
		textView.setText(""+purchase.getValue());
	}
}
