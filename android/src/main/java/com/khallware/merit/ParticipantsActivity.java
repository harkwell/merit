// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.domain.Participant;
import android.app.ProgressDialog;
import android.app.ListActivity;
import android.os.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParticipantsActivity extends ListActivity
		implements FetcherUiUpdater<Participant>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		ParticipantsActivity.class);
	private ParticipantsArrayAdapter adapter = null;
	private ProgressDialog dialog = null;

	@Override
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		adapter = new ParticipantsArrayAdapter(this);
		final FetcherUiUpdater updater = this;
		runOnUiThread(new Runnable() {
			public void run()
			{
				new ParticipantsFetcher(updater).execute();
			}
		});
	}

	@Override
	public void beforeFetch()
	{
		dialog = new ProgressDialog(this);
		dialog.setTitle("Participants");
		dialog.setMessage("Loading...");
		dialog.setIndeterminate(false);
		dialog.show();
	}

	@Override
	public void onItemFetch(Participant participant)
	{
		adapter.add(participant);
	}

	@Override
	public void afterFetch()
	{
		dialog.dismiss();
		setListAdapter(adapter);
	}
}
