// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.domain.Activity;
import android.app.ProgressDialog;
import android.app.ListActivity;
import android.os.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActivitiesActivity extends ListActivity
		implements FetcherUiUpdater<Activity>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		ActivitiesActivity.class);
	private ActivitiesArrayAdapter adapter = null;
	private ProgressDialog dialog = null;

	@Override
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		adapter = new ActivitiesArrayAdapter(this);
		final FetcherUiUpdater updater = this;
		runOnUiThread(new Runnable() {
			public void run()
			{
				new ActivitiesFetcher(updater).execute();
			}
		});
	}

	@Override
	public void beforeFetch()
	{
		dialog = new ProgressDialog(this);
		dialog.setTitle("Activities");
		dialog.setMessage("Loading...");
		dialog.setIndeterminate(false);
		dialog.show();
	}

	@Override
	public void onItemFetch(Activity activity)
	{
		adapter.add(activity);
	}

	@Override
	public void afterFetch()
	{
		dialog.dismiss();
		setListAdapter(adapter);
	}
}
