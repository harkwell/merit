// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.domain.Reward;
import android.app.ProgressDialog;
import android.app.ListActivity;
import android.os.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RewardsActivity extends ListActivity
		implements FetcherUiUpdater<Reward>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		RewardsActivity.class);
	private RewardsArrayAdapter adapter = null;
	private ProgressDialog dialog = null;

	@Override
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		adapter = new RewardsArrayAdapter(this);
		final FetcherUiUpdater updater = this;
		runOnUiThread(new Runnable() {
			public void run()
			{
				new RewardsFetcher(updater).execute();
			}
		});
	}

	@Override
	public void beforeFetch()
	{
		dialog = new ProgressDialog(this);
		dialog.setTitle("Rewards");
		dialog.setMessage("Loading...");
		dialog.setIndeterminate(false);
		dialog.show();
	}

	@Override
	public void onItemFetch(Reward reward)
	{
		adapter.add(reward);
	}

	@Override
	public void afterFetch()
	{
		dialog.dismiss();
		setListAdapter(adapter);
	}
}
