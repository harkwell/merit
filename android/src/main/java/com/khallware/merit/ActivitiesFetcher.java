// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.domain.Activity;
import com.jayway.jsonpath.JsonPath;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class ActivitiesFetcher extends MeritFetcher<Activity>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		ActivitiesFetcher.class);

	public ActivitiesFetcher(FetcherUiUpdater<Activity> updater)
	{
		super(updater);
	}

	@Override
	public String getURI()
	{
		return("/activities");
	}

	@Override
	public Activity parse(String json) throws MeritException
	{
                Activity.Builder retval = new Activity.Builder();
		try {
			retval.name(""+JsonPath.read(json, "$.name"));
			retval.description(
				""+JsonPath.read(json, "$.description"));
			retval.value(JsonPath.<Integer>read(json, "$.value"));
		}
		catch (Exception e) {
			throw new MeritException("bad json \""+json+"\"",e);
		}
		return(retval.build());
	}
}
