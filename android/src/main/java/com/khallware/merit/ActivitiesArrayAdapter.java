// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import com.khallware.merit.domain.Activity;
import android.view.View;
import android.widget.TextView;
import android.content.Context;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.List;
import java.util.ArrayList;

public class ActivitiesArrayAdapter extends MeritArrayAdapter<Activity>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		ActivitiesArrayAdapter.class);

	public ActivitiesArrayAdapter(Context ctxt)
	{
		super(ctxt, R.layout.activities_row, new ArrayList<Activity>());
	}

	@Override
	public void fillView(View view, Activity activity)
	{
		TextView textView = (TextView)view.findViewById(
			R.id.activity_list_row_name);
		textView.setText(""+activity.getName());

		textView = (TextView)view.findViewById(
			R.id.activity_list_row_desc);
		textView.setText(activity.getDescription());

		textView = (TextView)view.findViewById(
			R.id.activity_list_row_value);
		textView.setText(""+activity.getValue());
	}
}
