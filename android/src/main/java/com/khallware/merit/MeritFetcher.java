// Copyright Kevin D.Hall 2017

package com.khallware.merit;

import android.os.AsyncTask;
import java.net.HttpURLConnection;
import java.util.List;
import java.net.URL;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class MeritFetcher<T> extends AsyncTask<Void, Void, Void>
{
	private static final Logger logger = LoggerFactory.getLogger(	
		MeritFetcher.class);
	private static final String REST_URL =
		"http://192.168.23.103:8080/v0.1.0/merit";

	private FetcherUiUpdater updater = null;

	public MeritFetcher(FetcherUiUpdater updater)
	{
		this.updater = updater;
	}

	public abstract String getURI();

	public abstract T parse(String json) throws MeritException;

	@Override
	public void onPreExecute()
	{
		super.onPreExecute();
		updater.beforeFetch();
	}

	@Override
	protected Void doInBackground(Void... parms)
	{
		try {
			doInBackgroundUnwrapped();
		}
		catch (Exception e) {
			logger.error(""+e, e);
		}
		return(null);
	}

	private void doInBackgroundUnwrapped() throws Exception
	{
		HttpURLConnection connection = (HttpURLConnection)
			new URL(REST_URL+getURI()).openConnection();
		ByteArrayOutputStream bos = null;
		InputStream is = null;
		byte[] buffer = new byte[1024];
		connection.setRequestMethod("GET");
		logger.debug("connecting to {}", REST_URL+getURI());
		connection.connect();
		is = connection.getInputStream();
		bos = new ByteArrayOutputStream();
		   
		while (is.read(buffer) != -1) {
			bos.write(buffer);
		}
		for (String json : parseResult(new String(bos.toByteArray()))) {
			try {
				updater.onItemFetch(parse(json));
			}
			catch (MeritException e) {
				logger.error(""+e, e);
			}
		}
	}

	@Override
	protected void onPostExecute(Void result)
	{
		updater.afterFetch();
	}

	private static List<String> parseResult(String json)
			throws MeritException
	{
		int ptr1 = json.indexOf("[");
		int ptr2 = json.lastIndexOf("]");
		return(Util.flatJsonArrayParser(json.substring(ptr1,++ptr2)));
	}
}
